package com.kasidid.mrbboomm.calflog;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 10/14/2016.
 */

public class Menu_Farm_Fragment extends Fragment{
    COWHelper mHelper;
    Button reportFarm,editFarm,statusFarm;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().findViewById(R.id.search_list).setVisibility(View.GONE);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        final String username =  prefs.getString("username","not found");
        View view = inflater.inflate(R.layout.menu_farm_f, container, false);
        reportFarm = (Button)view.findViewById(R.id.reportfarm);
        reportFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),report_Farm.class);
                startActivity(intent);
            }
        });
        editFarm = (Button)view.findViewById(R.id.editfarm);
        editFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Edit_Add_Farm.class);
                startActivity(intent);
            }
        });
        statusFarm = (Button)view.findViewById(R.id.statusfarm);
        statusFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                new Sync_GetStatus(getActivity()).execute();
            }
        });
        return view;
    }
    public class Sync_GetStatus extends AsyncTask<String,Void,String> {
        Context mContext;
        String  data;
        ArrayList<String> status = new ArrayList<>();
        public Sync_GetStatus(Context context){
            this.mContext = context;
        }
        @Override
        protected String doInBackground(String... params) {
            SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(mContext);
            data= "?username=" + prefs.getString("username","not found");

            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("http://madlab.cpe.ku.ac.th/cowapp/report/status.php" + data).build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("result(status)",result);

                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);

                            status.add(x.getString("cow_count_header"));
                            status.add(x.getString("cow_count"));
                            status.add(x.getString("cow_milk305_header"));
                            status.add(x.getString("cow_milk305"));
                            status.add(x.getString("cow_milk_header"));
                            status.add(x.getString("cow_milk"));
                            status.add(x.getString("fret"));
                            Log.d("get(fromuser)", "done");

                        } catch (Exception e) {

                            Log.d("get(fromuser)","fail");
                            continue;
                        }

                    }
                }
                else{
                    return "none";
                }
                return "success";
            }catch (Exception e){
                Log.d("get(fromuser)","fail");
                return "fail";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            hideProgressDialog();
            if (s.equals("success")){

                Intent intent = new Intent(getActivity(),Farm_Status.class);
                intent.putExtra("status", status);
                startActivity(intent);

            }
            else{
                Toast.makeText(mContext, "ไม่สามารถทำรายการนี้ได้", Toast.LENGTH_LONG).show();

            }
        }
    }
    private ProgressDialog mProgressDialog;
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
}
