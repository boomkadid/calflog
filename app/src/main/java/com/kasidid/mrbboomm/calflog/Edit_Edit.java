package com.kasidid.mrbboomm.calflog;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mrbboomm on 8/19/2016.
 */
public class Edit_Edit extends AppCompatActivity implements com.layernet.thaidatetimepicker.date.DatePickerDialog.OnDateSetListener{
    private int ID = -1;
    COWHelper mHelper;
    CALVINGHelper cHelper = new CALVINGHelper(this);
    final int MY_PERMISSIONS_REQUEST_CAMERA = 44444;
    CameraPhoto cameraPhoto;
    GalleryPhoto galleryPhoto;
    final int CAMERA_REQUEST = 13322;
    final int GALLERY_REQUEST = 22132;
    private String img = null;
    ImageView imageView;
    ImageView ivCamera, ivGallery;
    static final int CAM_REQUEST = 1;
    private ProgressDialog mProgressDialog;
    String id;
    int birthdate_request=564564,daystart_request= 456464,request_datetime=54564646,calvingdate_request=434856345;
    Bitmap bitbit;
    String selectedPhoto,status="",username;
    EditText etNickname,etDetail,etBirth,etcalvingdate,etpapa,etpapaf,etmama,etmamaf,daystart,times,couple;
    Cow c;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        username =  prefs.getString("username","not found");
        setContentView(R.layout.edit_edit);
        mHelper = new COWHelper(this);
        Intent intent = getIntent();
        id = intent.getStringExtra("ID");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_edit);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        etNickname = (EditText) findViewById(R.id.cow_nickname_edit);
        etDetail = (EditText) findViewById(R.id.cow_detail_edit);
        etBirth = (EditText) findViewById(R.id.txtdate_edit);
        etcalvingdate = (EditText) findViewById(R.id.calvingdate_edit);
        etpapa = (EditText) findViewById(R.id.cow_papa_edit);
        etpapaf = (EditText) findViewById(R.id.cow_papaf_edit);
        etmama = (EditText) findViewById(R.id.cow_mama_edit);
        etmamaf = (EditText) findViewById(R.id.cow_mamaf_edit);
        daystart = (EditText) findViewById(R.id.daystart);
        times = (EditText) findViewById(R.id.times);
        couple = (EditText) findViewById(R.id.couple);

        imageView = (ImageView) findViewById(R.id.image_cow);
        ivCamera = (ImageView)findViewById(R.id.ivCamera);
        ivGallery = (ImageView)findViewById(R.id.ivGallery);
        cameraPhoto = new CameraPhoto(getApplicationContext());
        galleryPhoto = new GalleryPhoto(getApplicationContext());
        c = mHelper.getCowByID(id);
        etNickname.setText(c.getNickname());
        etDetail.setText(c.getFraction());
        etBirth.setText(c.getBirth());
        etpapa.setText(c.getPapa());
        etpapaf.setText(c.getPapaf());
        etmama.setText(c.getMama());
        etmamaf.setText(c.getMamaf());
        Calving calving = cHelper.getCalving(c.getCowname(),c.getParity());
        try {
            etcalvingdate.setText(calving.getCalvingdate());
        }catch (Exception e){
            etcalvingdate.setText(c.getCalvdate());
        }
        Picasso.with(this).load("http://madlab.cpe.ku.ac.th/cowapp/manager/pictures/"+username+"/"+c.getCowname()+"/1.jpg").into(imageView);
        final Spinner spinner = (Spinner) findViewById(R.id.status);

        ArrayAdapter<String> adapter2 =
                new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{"เลือก", "รอผสมพันธุ์","ผสมพันธุ์แล้ว"});
        spinner.setAdapter(adapter2);
        final TextView question = (TextView)findViewById(R.id.daystart_);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) view).setTextSize(22);
                if (i == 1){
                    ((TextView) findViewById(R.id.couple_)).setTextColor(Color.GRAY);
                    ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.GRAY);
                    ((TextView) findViewById(R.id.times_)).setTextColor(Color.GRAY);
                    daystart.setEnabled(false);
                    times.setEnabled(false);
                    couple.setEnabled(false);
                    question.setEnabled(false);
                    status = "0";
                    daystart.setText("");
                }
                else if (i == 2){
                    daystart.setEnabled(true);
                    times.setEnabled(true);
                    couple.setEnabled(true);
                    question.setEnabled(true);
                    question.setText("ผสมเมื่อวันที่*: ");
                    ((TextView) findViewById(R.id.couple_)).setTextColor(Color.BLACK);
                    ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.BLACK);
                    ((TextView) findViewById(R.id.times_)).setTextColor(Color.BLACK);
                    daystart.setHint("วันที่ผสม");
                    status = "2";

                }
                else{
                    ((TextView) findViewById(R.id.couple_)).setTextColor(Color.GRAY);
                    ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.GRAY);
                    ((TextView) findViewById(R.id.times_)).setTextColor(Color.GRAY);
                    daystart.setEnabled(false);
                    times.setEnabled(false);
                    couple.setEnabled(false);
                    question.setEnabled(false);
                    status = "";
                    daystart.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        etBirth.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = birthdate_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Edit.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        etcalvingdate.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = calvingdate_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Edit.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        daystart.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = daystart_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Edit.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        //======================================================================

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(Edit_Edit.this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
        });

        ivGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                Log.i("Camera", "G : " + grantResults[0]);
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    openCamera();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

                        showAlert();

                    } else {
                    }
                }
                return;
            }

        }
    }
    private void showAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(Edit_Edit.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(Edit_Edit.this,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);

                    }
                });
        alertDialog.show();
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                bitbit = photo;
                imageView.setImageBitmap(photo);

            } else if (requestCode == GALLERY_REQUEST) {
                Uri uri = data.getData();
                galleryPhoto.setPhotoUri(uri);
                String photoPath = galleryPhoto.getPath();
                selectedPhoto = photoPath;
                Bitmap bitmap = null;
                try {
                    bitmap = ImageLoader.init().from(selectedPhoto).requestSize(1024, 1024).getBitmap();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitbit = bitmap;
                imageView.setImageBitmap(bitmap);
            }
        }
    }
    private void openCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }
    @Override
    public void onDateSet(com.layernet.thaidatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        if (request_datetime == birthdate_request) etBirth.setText(date);
        if (request_datetime == daystart_request) daystart.setText(date);
        if (request_datetime == calvingdate_request) etcalvingdate.setText(date);
    }
    public class Sync_DeleteCow extends AsyncTask<String, Void, String> {
        Context mContext;
        COWHelper mHelper;
        Cow mcow;

        public Sync_DeleteCow(Context context, Cow cow) {
            mContext = context;
            mcow = cow;
        }

        protected String doInBackground(String... params) {
            String link;
            String data;
            try {
                data = "?username=" + URLEncoder.encode(mcow.getUsername(), "UTF-8");
                data += "&cowname=" + URLEncoder.encode(mcow.getCowname(), "UTF-8");
                link = "http://madlab.cpe.ku.ac.th/cowapp/manager/delete.php" + data;
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                return "success";


            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        protected void onPostExecute(String result) {
            if (result == "success") {
                mHelper = new COWHelper(Edit_Edit.this);
                mHelper.deleteCow(id);
//                Intent intent = new Intent(Edit_Edit.this, Menu_Main.class);
//                startActivity(intent);
//                finish();
            }
            else{
                Toast.makeText(Edit_Edit.this,"Deleted Failed.",Toast.LENGTH_SHORT).show();
            }
        }


    }
    public class Sync_DeleteCalving extends AsyncTask<String, Void, String> {
        Context mContext;
        COWHelper mHelper;
        Cow mcow;

        public Sync_DeleteCalving(Context context, Cow cow) {
            mContext = context;
            mcow = cow;
        }

        protected String doInBackground(String... params) {
            String link;
            String data;
            try {
                data = "?username=" + URLEncoder.encode(mcow.getUsername(), "UTF-8");
                data += "&cowname=" + URLEncoder.encode(mcow.getCowname(), "UTF-8");
                link = "http://madlab.cpe.ku.ac.th/cowapp/manager/deletecalving.php" + data;
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                return "success";


            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        protected void onPostExecute(String result) {
            if (result == "success") {
                cHelper.deleteCalving(c.getCowname());
                Intent intent = new Intent(Edit_Edit.this, Menu_Main.class);
                startActivity(intent);
                finish();
            }
            else{
                Toast.makeText(Edit_Edit.this,"Deleted Failed.",Toast.LENGTH_SHORT).show();
            }
        }


    }
    public void editclick(View view){
        if(status.equals("2") && (couple.getText().toString().equals("") || times.getText().toString().equals("") || daystart.getText().toString().equals("") )){
            new AlertDialog.Builder(Edit_Edit.this)
                    .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return;
        }
        c.setNickname(etNickname.getText().toString());
        c.setFraction(etDetail.getText().toString());
        c.setBirth(etBirth.getText().toString());
        c.setPapa(etpapa.getText().toString());
        c.setPapaf(etpapaf.getText().toString());
        c.setMama(etmama.getText().toString());
        c.setMamaf(etmamaf.getText().toString());
        c.setCalvdate(etcalvingdate.getText().toString());
        c.getEndDate(); ////////////////////////////////////////////TESTER

        if (status.equals("2")){
            c.setStatus(status);
            c.setDaystart(daystart.getText().toString());
//            Breed b = new Breed();
//            b.setCowname(c.getCowname());
//            b.setParity(String.valueOf(Integer.parseInt(c.getParity())+1));
//            b.setPapa(couple.getText().toString());
//            b.setBreeddate(daystart.getText().toString());
//            b.setUsername(username);
//            for (int t = 0;t<Integer.parseInt(times.getText().toString());t++){
//                new BREEDHelper(Edit_Edit.this).addBreed(b);
//                new Sync_Breed(b).execute();
//            }

//                    ##############################################################################################
            CBreed cBreed = new CBreed();
            cBreed.setCowname(c.getCowname());
            cBreed.setUsername(username);
            cBreed.setTimes(times.getText().toString());
            cBreed.setPapa(couple.getText().toString());
            cBreed.setBreeddate(daystart.getText().toString());
            cBreed.setParity(String.valueOf(Integer.parseInt(c.getParity())+1));
            new CBREEDHelper(Edit_Edit.this).addCBreed(cBreed);
            new Sync_CBreed(cBreed).execute();
//                    ###############################################################################################
//                    ###############################################################################################
        }
        else if(status.equals("0")){
            c.setStatus(status);
            c.setDaystart(Calendar.getInstance().get(Calendar.YEAR)+"-"+(Calendar.getInstance().get(Calendar.MONTH)+1)+"-"+Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        }
        if (!(c.getStatus().equals("3") && c.getState().equals("1"))) c.setState("0");
        if (bitbit != null){
            String encodedImage = ImageBase64.encode(bitbit);

            HashMap<String, String> postData = new HashMap<String, String>();
            postData.put("image", encodedImage);
            postData.put("name", c.getCowname());
            postData.put("username", username);
            Log.d("image",encodedImage);
            new pushbyPost(Edit_Edit.this,"https://madlab.cpe.ku.ac.th/cowapp/manager/uploadImage2.php",postData).execute();
        }
        if (ID == -1) {
            mHelper.updateCow(mHelper.StateUpdate(c));
            try {Log.d("edit", "update");
                Calving calving = cHelper.getCalving(c.getCowname(),c.getParity());
                calving.setCalvingdate(etcalvingdate.getText().toString());
                cHelper.updateCalving(calving);
                new Sync_Calving(calving).execute();

            }catch (Exception e){ //not found
                Log.d("edit", "add new");
                Calving calving = new Calving();
                calving.setCowname(c.getCowname());
                calving.setCalvingdate(etcalvingdate.getText().toString());
                calving.setUsername(c.getUsername());
                calving.setRestday("-");
                calving.setKid("-");
                cHelper.addCalving(calving);
                new Sync_Calving(calving).execute();
            }

        } else {
            c.setId(ID);
        }
        new Sync_DB(c).execute();
        finish();

    }
    public void deleteclick(View view){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(Edit_Edit.this);
        builder.setMessage("ต้องการลบโค "+c.getCowname()+" ?");
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int idd) {
                mHelper.deleteCow(String.valueOf(c.getId()));
                new Sync_DeleteCow(Edit_Edit.this, c).execute();
                new Sync_DeleteCalving(Edit_Edit.this, c).execute();
                cHelper.deleteCalving(c.getCowname());

            }
        });
        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}


