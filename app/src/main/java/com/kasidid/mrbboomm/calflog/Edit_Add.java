package com.kasidid.mrbboomm.calflog;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mrbboomm on 8/19/2016.
 */
public class Edit_Add extends AppCompatActivity implements com.layernet.thaidatetimepicker.date.DatePickerDialog.OnDateSetListener{
    private final String TAG = this.getClass().getName();
    private int ID = -1;
    COWHelper mHelper;
    String username,type,status="";
    private Calendar today;
    ImageView ivCamera, ivGallery,imageView;
    EditText txtDate,etcalvingdate;
    CameraPhoto cameraPhoto;
    GalleryPhoto galleryPhoto;
    String selectedPhoto;
    EditText etCowName,etNickName,etFraction,etBirth,etpapaf,etmama,etmamaf,etparity,daystart,times,couple;
    Button next_page1,next_page2,next_page3,next_page4,previous_page1,previous_page2,previous_page3,previous_page4,previous_page5;
    final int MY_PERMISSIONS_REQUEST_CAMERA = 33333,date_request = 154564,calving_request = 78964156,GALLERY_REQUEST = 22131,CAMERA_REQUEST = 13323,daystart_request=487035;
    int request_datetime=00000;
    AutoCompleteTextView etpapa;
    Typeface tf;
    Bitmap bitbit;
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private ProgressDialog mProgressDialog;

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("คุณต้องการออกจากหน้านี้?")
                .setNegativeButton("ยกเลิก", null)
                .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                }).create().show();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_add);
        String fontPath = "fonts/THSarabun.ttf";
        tf = Typeface.createFromAsset(getAssets(),fontPath);
        mHelper = new COWHelper(this);
                Intent intent = getIntent();
        type = intent.getStringExtra("type");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_add);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("เพิ่มข้อมูลโครีด");
        findViewById(R.id.page4).setVisibility(View.GONE);
        if (type.equals("0")){
            getSupportActionBar().setTitle("เพิ่มข้อมูลโคทดแทน");
            findViewById(R.id.parity_).setVisibility(View.INVISIBLE);
            findViewById(R.id.parity).setVisibility(View.INVISIBLE);
            findViewById(R.id.calvingdate_).setVisibility(View.GONE);
            findViewById(R.id.page3).setVisibility(View.GONE);
        }
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        username =  prefs.getString("username","not found");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(Edit_Add.this)
                        .setMessage("คุณต้องการออกจากหน้านี้?")
                        .setNegativeButton("ยกเลิก", null)
                        .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).create().show();
            }
        });

        etCowName = (EditText) findViewById(R.id.cow_name);
        etNickName = (EditText) findViewById(R.id.cow_nickname);
        etFraction = (EditText) findViewById(R.id.cow_detail);
        etBirth = (EditText) findViewById(R.id.txtdate);
        daystart = (EditText) findViewById(R.id.daystart);
        times = (EditText) findViewById(R.id.times);
        couple = (EditText) findViewById(R.id.couple);
        //////////////////////auto complete
        etpapa = (AutoCompleteTextView) findViewById(R.id.cow_papa);
        List<String> papas = mHelper.getCowSearchArray(username,10,0,false);
        papas.addAll(mHelper.getCowArray(username,10,1,false));
        Set<String> hs = new HashSet<>();
        hs.addAll(papas);
        papas.clear();
        papas.addAll(hs);
        etpapa.setTypeface(tf);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,papas );
        etpapa.setAdapter(adapter);
        ///////////////////////////spinner
        final Spinner spinner = (Spinner) findViewById(R.id.status);
        ArrayAdapter<String> adapter2 =
                new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{"เลือก", "รอผสมพันธุ์","ผสมพันธุ์แล้ว"});
        spinner.setAdapter(adapter2);
        final TextView question = (TextView)findViewById(R.id.daystart_);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) view).setTextSize(22);
                if (i == 1){
                    ((TextView) findViewById(R.id.couple_)).setTextColor(Color.GRAY);
                    ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.GRAY);
                    ((TextView) findViewById(R.id.times_)).setTextColor(Color.GRAY);
                    daystart.setEnabled(false);
                    times.setEnabled(false);
                    couple.setEnabled(false);
                    question.setEnabled(false);
                    status = "0";
                    daystart.setText("");
                }
                else if (i == 2){
                    daystart.setEnabled(true);
                    etcalvingdate.clearFocus();
                    times.setEnabled(true);
                    couple.setEnabled(true);
                    question.setEnabled(true);
                    question.setText("ผสมเมื่อวันที่*: ");
                    ((TextView) findViewById(R.id.couple_)).setTextColor(Color.BLACK);
                    ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.BLACK);
                    ((TextView) findViewById(R.id.times_)).setTextColor(Color.BLACK);
                    daystart.setHint("วันที่ผสม");
                    status = "2";

                }
                else{
                    ((TextView) findViewById(R.id.couple_)).setTextColor(Color.GRAY);
                    ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.GRAY);
                    ((TextView) findViewById(R.id.times_)).setTextColor(Color.GRAY);
                    daystart.setEnabled(false);
                    times.setEnabled(false);
                    couple.setEnabled(false);
                    question.setEnabled(false);
                    status = "";
                    daystart.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        daystart.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = daystart_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Add.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        //////////////////////////////////////////
        etpapaf = (EditText) findViewById(R.id.cow_papaf);
        etmama = (EditText) findViewById(R.id.cow_mama);
        etmamaf = (EditText) findViewById(R.id.cow_mamaf);
         etparity = (EditText) findViewById(R.id.parity);
        etcalvingdate = (EditText) findViewById(R.id.calvingdate);
        txtDate = (EditText)findViewById(R.id.txtdate);
        ivCamera = (ImageView)findViewById(R.id.ivCamera);
        ivGallery = (ImageView)findViewById(R.id.ivGallery);
        imageView = (ImageView) findViewById(R.id.image_cow);
        cameraPhoto = new CameraPhoto(getApplicationContext());
        galleryPhoto = new GalleryPhoto(getApplicationContext());

        //=====================================================================
        page_handle();

        etcalvingdate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = calving_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Add.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                );
                dpd.setAccentColor("#6388ad");
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }}
        });
        txtDate.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = date_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Add.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(Edit_Add.this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
        });
        ivGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
            }
        });
        Button bDone = (Button) findViewById(R.id.cowadd_button);
        bDone.setTypeface(tf);
        today = Calendar.getInstance();
    }

    private void page_handle() {
        //////////////////////////////////////////////////////////////////////
        next_page1 = (Button)findViewById(R.id.next_page1);
        previous_page1 = (Button)findViewById(R.id.previous_page1);
        next_page2 = (Button)findViewById(R.id.next_page2);
        previous_page2 = (Button)findViewById(R.id.previous_page2);
        next_page3 = (Button)findViewById(R.id.next_page3);
        previous_page3 = (Button)findViewById(R.id.previous_page3);
        next_page4 = (Button)findViewById(R.id.next_page4);
        previous_page4 = (Button)findViewById(R.id.previous_page4);
        previous_page5 = (Button)findViewById(R.id.previous_page5);
        next_page1.setTypeface(tf);next_page2.setTypeface(tf);next_page3.setTypeface(tf);next_page4.setTypeface(tf);
        previous_page1.setTypeface(tf);previous_page2.setTypeface(tf);previous_page3.setTypeface(tf);previous_page4.setTypeface(tf);previous_page5.setTypeface(tf);

        next_page1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etCowName.getText().toString().equals("") || etBirth.getText().toString().equals("")){
                    new AlertDialog.Builder(Edit_Add.this)
                            .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                List<String> cows_id = mHelper.getCowArray(username,2,1,true);
                cows_id.addAll(mHelper.getCowArray(username,2,0,true));
                if(cows_id.contains(etCowName.getText().toString())){
                    new AlertDialog.Builder(Edit_Add.this)
                            .setMessage("หมายเลขโค "+etCowName.getText().toString()+" ถูกใช้งานไปแล้ว")
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                findViewById(R.id.page1).setVisibility(View.GONE);
                findViewById(R.id.page2).setVisibility(View.VISIBLE);
            }
        });
        previous_page1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        next_page2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.page2).setVisibility(View.GONE);
                findViewById(R.id.page3).setVisibility(View.VISIBLE);
            }
        });
        previous_page2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.page1).setVisibility(View.VISIBLE);
                findViewById(R.id.page2).setVisibility(View.GONE);
            }
        });
        next_page3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("0")){
                    findViewById(R.id.page3).setVisibility(View.GONE);
                    findViewById(R.id.page5).setVisibility(View.VISIBLE);
                }
                else {
                    findViewById(R.id.page3).setVisibility(View.GONE);
                    findViewById(R.id.page4).setVisibility(View.VISIBLE);
                }
            }
        });
        previous_page3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.page2).setVisibility(View.VISIBLE);
                findViewById(R.id.page3).setVisibility(View.GONE);
            }
        });
        next_page4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((etparity.getText().toString().equals("") || etcalvingdate.getText().toString().equals("")) && type.equals("1"))){
                    new AlertDialog.Builder(Edit_Add.this)
                            .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                Calendar day_a = Calendar.getInstance();
                try {
                    day_a.setTime(df.parse(etcalvingdate.getText().toString()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (today.getTimeInMillis()-day_a.getTimeInMillis()<0){
                    Cow cow = new Cow();
                    new AlertDialog.Builder(Edit_Add.this)
                            .setMessage("ไม่สามารถกรอกวันคลอดหลังจากวันที่ "+ cow.getDayTH((today.get(Calendar.YEAR)+"-"+(today.get(Calendar.MONTH)+1)+"-"+today.get(Calendar.DAY_OF_MONTH))))
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                findViewById(R.id.page4).setVisibility(View.GONE);
                findViewById(R.id.page5).setVisibility(View.VISIBLE);
            }
        });
        previous_page4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.page3).setVisibility(View.VISIBLE);
                findViewById(R.id.page4).setVisibility(View.GONE);
            }
        });
        previous_page5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("0")) {
                    findViewById(R.id.page3).setVisibility(View.VISIBLE);
                    findViewById(R.id.page5).setVisibility(View.GONE);
                }
                else{
                    findViewById(R.id.page4).setVisibility(View.VISIBLE);
                    findViewById(R.id.page5).setVisibility(View.GONE);
                }
            }
        });
        ///////////////////////////////////////////////////////////////////////////
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(requestCode == CAMERA_REQUEST){
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                bitbit = photo;
                imageView.setImageBitmap(photo);

            }
            else if(requestCode == GALLERY_REQUEST){
                Uri uri = data.getData();
                galleryPhoto.setPhotoUri(uri);
                String photoPath = galleryPhoto.getPath();
                selectedPhoto = photoPath;
                Bitmap bitmap = null;

                try {
                     bitmap = ImageLoader.init().from(selectedPhoto).requestSize(1024,1024).getBitmap();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitbit = bitmap;
                imageView.setImageBitmap(bitmap);
            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                Log.i("Camera", "G : " + grantResults[0]);
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    openCamera();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

                        showAlert();

                    } else {
                    }
                }
                return;
            }

        }
    }
    private void showAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(Edit_Add.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(Edit_Add.this,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);

                    }
                });
        alertDialog.show();
    }
    private void openCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }
    @Override
    public void onDateSet(com.layernet.thaidatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        if (request_datetime == date_request) txtDate.setText(date);
        if (request_datetime == calving_request) etcalvingdate.setText(date);
        if (request_datetime == daystart_request) daystart.setText(date);
        Log.d("code", String.valueOf(request_datetime));
    }
    public void addclick(View view){
        if(status.equals("")){
            new AlertDialog.Builder(Edit_Add.this)
                    .setMessage("กรุณาเลือกสถานะ")
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return;
        }


        if(status.equals("2") && (daystart.getText().toString().equals("") || couple.getText().toString().equals("") || times.getText().toString().equals(""))){
            new AlertDialog.Builder(Edit_Add.this)
                    .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return;
        }
        Calendar day_a = Calendar.getInstance();
        try {
            day_a.setTime(df.parse(daystart.getText().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (today.getTimeInMillis()-day_a.getTimeInMillis()<0 && status.equals("2")){
            Cow cow = new Cow();
            new AlertDialog.Builder(Edit_Add.this)
                    .setMessage("ไม่สามารถกรอกวันผสมหลังจากวันที่ "+ cow.getDayTH((today.get(Calendar.YEAR)+"-"+(today.get(Calendar.MONTH)+1)+"-"+today.get(Calendar.DAY_OF_MONTH))))
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return;
        }
        AlertDialog.Builder builder =
                new AlertDialog.Builder(Edit_Add.this);
        builder.setMessage("ต้องการเพิ่มข้อมูลของโค "+etNickName.getText().toString()+" ?");
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Cow cow = new Cow();
                cow.setCowname(etCowName.getText().toString());
                if (etNickName.getText().toString().equals("")) cow.setNickname(etCowName.getText().toString());
                else cow.setNickname(etNickName.getText().toString());
                cow.setFraction(etFraction.getText().toString());
                cow.setBirth(etBirth.getText().toString());
                cow.setUsername(username);
                cow.setStatus(status);
                cow.setPapa(etpapa.getText().toString());
                cow.setPapaf(etpapaf.getText().toString());
                cow.setMama(etmama.getText().toString());
                cow.setMamaf(etmamaf.getText().toString());
                cow.setType(type.toString());
                cow.setState("0");
                if (type.equals("0")){
                    cow.setParity("0");
                    cow.setCalvdate("-");

                }
                else{
                    cow.setParity(etparity.getText().toString());
                    cow.setCalvdate(etcalvingdate.getText().toString());
                    if (!cow.getParity().equals("0")){
                        Calving calving = new Calving();
                        calving.setUsername(cow.getUsername());
                        calving.setCowname(cow.getCowname());
                        calving.setParity(cow.getParity());
                        calving.setCalvingdate(cow.getCalvdate());
                        calving.setRestday("-");
                        calving.setKid("-");
                        CALVINGHelper calvingHelper = new CALVINGHelper(Edit_Add.this);
                        calvingHelper.addCalving(calving);
                        new Sync_Calving(calving).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }

                if (bitbit != null){
                    String encodedImage = ImageBase64.encode(bitbit);

                    HashMap<String, String> postData = new HashMap<String, String>();
                    postData.put("image", encodedImage);
                    postData.put("name", etCowName.getText().toString());
                    postData.put("username", username);
                    new pushbyPost(Edit_Add.this,"https://madlab.cpe.ku.ac.th/cowapp/manager/uploadImage2.php",postData).execute();
                }
                if (status.equals("2")){
                    cow.setDaystart(daystart.getText().toString());
                    CBreed cBreed = new CBreed();
                    cBreed.setCowname(cow.getCowname());
                    cBreed.setUsername(username);
                    cBreed.setTimes(times.getText().toString());
                    cBreed.setPapa(couple.getText().toString());
                    cBreed.setBreeddate(daystart.getText().toString());
                    cBreed.setParity(String.valueOf(Integer.parseInt(cow.getParity())+1));
                    new CBREEDHelper(Edit_Add.this).addCBreed(cBreed);
                    new Sync_CBreed(cBreed).execute();

                }
                else{
                    cow.setDaystart(today.get(Calendar.YEAR)+"-"+(today.get(Calendar.MONTH)+1)+"-"+today.get(Calendar.DAY_OF_MONTH));
                }
                if (ID == -1) {
                    mHelper.addCow(mHelper.StateUpdate(cow));

                } else {
                    cow.setId(ID);
                }
                new Sync_DB(cow).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);;
                finish();
            }
        });
        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
    public class Sync_Breeds extends AsyncTask<String, Void, String> {
        private String username,cowname,parity,papa,breedate;

        public Sync_Breeds(String username, String cowname, String parity,String papa, String breedate) {
            this.username=username;
            this.cowname=cowname;
            this.parity=parity;
            this.breedate=breedate;
            this.papa = papa;
        }

        @Override
        protected String doInBackground(String... strings) {
            String link;
            String data;
            try {
                data = "?username=" + this.username;
                data += "&cowname=" + this.cowname;
                data += "&parity=" + this.parity;
                data += "&papa=" + this.papa;
                data += "&breeddate=" + this.breedate;
                link = "http://madlab.cpe.ku.ac.th/cowapp/manager/updateallbreeds.php" + data;
                link = link.replaceAll(" ","__");
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                Log.d("link_breed", link);
                return response.body().string();

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("link", "FAIL sync");
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            String jsonStr = result;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("query_result");
                    if (query_result.equals("SUCCESS")) {
                        Log.d("result_sync", "SUCESS");
                    } else if (query_result.equals("FAILURE")) {
                        Log.d("result_sync", "FAILURE");
                    } else {

                        Log.d("result_sync", "Couldn't connect");
                    }
                } catch (JSONException e) {
                    Log.d("result_sync", result);
                }
            } else {
                Log.d("result_sync", "couldn't get JSON data");
            }
        }

    }
}


