package com.kasidid.mrbboomm.calflog;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by mrbboomm on 7/18/2016.
 */
public class Menu_Activity_Fragment extends Fragment implements  SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener{
    ExpandableListView exlist;
    SavedTabsListAdapter adapt;
    List<String> cows_id,cows_nickname;
    String[][] p;
    String username;
    COWHelper mHelper;
    @Override
    public void onResume() {
        super.onResume();

        getActivity().findViewById(R.id.search_list).setVisibility(View.GONE);
        p = mHelper.getCowNot(username);
        adapt = new SavedTabsListAdapter(getActivity(),p);
        exlist.setAdapter(adapt);
        exlist.expandGroup(0);
        exlist.expandGroup(1);
        adapt.notifyDataSetChanged();
        exlist.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Intent intent;
                intent = new Intent(getActivity(), Activity_Main.class);
                intent.putExtra("ID", p[groupPosition][childPosition]);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                return false;
            }
        });
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        getActivity().getMenuInflater().inflate(R.menu.menu_search,menu);
        final MenuItem item = menu.findItem(R.id.menuSearch);
        final SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                query = null;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                final String[] ids;
                if (newText.length() == 0){
                    cows_id = mHelper.getCowArray(username,0,1,true);
                    p = mHelper.getCowNot(username);
                    adapt = new SavedTabsListAdapter(getActivity(),p);
                    exlist.setAdapter(adapt);
                    exlist.expandGroup(0);
                    exlist.expandGroup(1);
                    exlist.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                        @Override
                        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                            Intent intent;
                            intent = new Intent(getActivity(), Activity_Main.class);
                            intent.putExtra("ID", p[groupPosition][childPosition]);
                            getActivity().startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                            return false;
                        }
                    });
                }
                else{
                    List<String> cows_filter = new ArrayList<String>();
                    cows_id = mHelper.getCowArray(username,0,1,true);
                    cows_id.addAll(mHelper.getCowArray(username,0,0,true));
                    cows_nickname = mHelper.getCowArray(username,15,1,true);
                    cows_nickname.addAll(mHelper.getCowArray(username,15,0,true));
                    for(int i=0;i<cows_nickname.size();i++)
                    {
                        if(cows_nickname.get(i).toLowerCase().startsWith(newText.toLowerCase()))
                        {
                            cows_filter.add(cows_id.get(i));
                        }
                    }
                    ids = cows_filter.toArray(new String[0]);
                    adapt = new SavedTabsListAdapter(getActivity(),new String[][]{ids,new String[]{}});

                    exlist.setAdapter(adapt);
                    exlist.expandGroup(0);
                    exlist.expandGroup(1);
                    exlist.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                        @Override
                        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                            Intent intent;

                            intent = new Intent(getActivity(), Activity_Main.class);
                            intent.putExtra("ID", ids[childPosition]);
                            getActivity().startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                            return false;
                        }
                    });
                }

//                exlist.expandGroup(1);
//                adapt.notifyDataSetChanged();
//                mRecyclerView.setAdapter(mAdapter);
                return false;
            }

        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        username =  prefs.getString("username","not found");
        View view = inflater.inflate(R.layout.menu_activity_f, container, false);
        exlist = (ExpandableListView )view.findViewById(R.id.listcow_Activity);

        exlist.setChildDivider(getResources().getDrawable(R.color.white));
        exlist.setDivider(getResources().getDrawable(R.color.white));
        exlist.setDividerHeight(2);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.userprofile_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("สถานะ");
        mHelper = new COWHelper(getActivity());
        p = mHelper.getCowNot(username);
        adapt = new SavedTabsListAdapter(getActivity(),p);

        return view;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return false;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        return false;
    }

    public class SavedTabsListAdapter extends BaseExpandableListAdapter {
        Context mcontext;
        COWHelper mHelper;
        private String[] groups = { "ตรวจสอบ", "ทั่วไป"};
        private String[][] children;


        public SavedTabsListAdapter(Context context, String[][] p) {
            this.mcontext = context;
            this.children = p;
        }


        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();

        }

        @Override
        public int getGroupCount() {
            return groups.length;
        }

        @Override
        public int getChildrenCount(int i) {
            return children[i].length;
        }

        @Override
        public Object getGroup(int i) {
            return groups[i];
        }

        @Override
        public Object getChild(int i, int i1) {
            return children[i][i1];
        }

        @Override
        public long getGroupId(int i) {
            return i;
        }

        @Override
        public long getChildId(int i, int i1) {
            return i1;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

            if (view == null) {
                LayoutInflater mInflater =
                        (LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = mInflater.inflate(R.layout.menu_activity_group, null);
            }
            TextView textView = (TextView)view.findViewById(R.id.groupname);
            textView.setText(getGroup(i).toString()+" ("+children[i].length+")");
            return view;
        }
        @Override
        public View getChildView(int i, int i1, boolean bool, View view, ViewGroup viewGroup) {
            LayoutInflater mInflater =
                    (LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.menu_activity_item, null);
            mHelper = new COWHelper(mcontext);
            final Cow  c = mHelper.getCowByID(children[i][i1]);
            TextView cowname = (TextView)view.findViewById(R.id.cowname);
            if (c.getNickname().equals("")) cowname.setText(c.getCowname());
            else cowname.setText(c.getNickname());
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView1_act);
            Picasso.with(getActivity()).load("https://madlab.cpe.ku.ac.th/cowapp/manager/images/"+c.getUsername()+"/"+c.getCowname()+"/1.png").error(R.drawable.cow_acc).into(imageView);

            TextView Date = (TextView)view.findViewById(R.id.date);
            Date.setText(c.getDaystart()+" ถึง "+c.getDayend());

            TextView Status = (TextView)view.findViewById(R.id.status);
            Status.setText(c.showStatus());

            TextView priority = (TextView)view.findViewById(R.id.priority);

            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            switch(c.getStatus()){
                case "0":{
                    priority.setText("-");
                    break;
                }
                case "1":{
                    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH");
                    Calendar mark = Calendar.getInstance();
                    Calendar starta = Calendar.getInstance();
                    Calendar enda = Calendar.getInstance();
                    Calendar noon = Calendar.getInstance();
                    noon.set(Calendar.HOUR_OF_DAY, 12);
                    try {
                        mark.setTime(df2.parse(c.getState()));
                        starta.setTime(df2.parse(c.getState()));
                        enda.setTime(df2.parse(c.getState()));
                        enda.add(Calendar.HOUR,2);
                    } catch (ParseException e) {
                    }
                    if(c.getState().equals("0") || c.getState()==null){
                        priority.setText("ตรวจสอบการเป็นสัด");
                        priority.setTextColor(Color.RED);
                        break;
                    }
                    if (c1.before(enda) && c1.after(starta)){
                        priority.setText("พร้อมผสมพันธุ์");
                        priority.setTextColor(Color.RED);
                    }
                    else if(c1.after(starta)){
                        if (mark.before(noon) && c1.before(noon))
                            priority.setText("ควรผสมแม่โคภายในเที่ยงนี้");
                        else
                            priority.setText("ควรผสมแม่โคภายในวันนี้");
                        priority.setTextColor(Color.RED);
                    }
                    else{
                        if (starta.get(Calendar.DAY_OF_YEAR) == c1.get(Calendar.DAY_OF_YEAR))
                            priority.setText("ผสมแม่โควันนี้"+ " ช่วงเวลา "+starta.get(Calendar.HOUR_OF_DAY)+"-"+(enda.get(Calendar.HOUR_OF_DAY))+" นาฬิกา");
                        else
                            priority.setText("ผสมแม่โควันพรุ่งนี้"+ " ช่วงเวลา "+starta.get(Calendar.HOUR_OF_DAY)+"-"+(enda.get(Calendar.HOUR_OF_DAY))+" นาฬิกา");
                        priority.setTextColor(Color.RED);
                    }
                    break;
                }
                case "2":{
                    if(c.getState()==null){
                        priority.setText(null);
                        priority.setTextColor(Color.RED);
                        break;
                    }
                    if(c.getState().equals("1") || c.getState().equals("2") || c.getState().equals("3")){
                        priority.setText("ตรวจสอบการกลับสัดครั้งที่ "+c.getState());
                        priority.setTextColor(Color.RED);
                    }else{
                        try {
                            c1.setTime(df.parse(c.getState()));
                            c2.setTime(df.parse(c.getState()));
                        } catch (ParseException e) {
//                            e.printStackTrace();
                        }
                        c2.add(Calendar.DATE,6);
                        priority.setText("รอตรวจสอบการกลับสัด");
                        priority.setTextColor(Color.parseColor("#E69500"));
                    }
                    break;
                }
                case "3":{

                    if(c.getState().equals("0")) {
                        priority.setText("เรียกหมอตรวจท้อง");
                        priority.setTextColor(Color.RED);
                    }
                    else if (c.getState().equals("1")){
                        priority.setText("คลอดวันที่ "+c.getDayTH(c.getDayend()));
                        priority.setTextColor(Color.parseColor("#00b200"));
                    }
                    else if (c.getState().equals("2")){
                        priority.setText("อยู่ในระยะใกล้คลอด");
                        priority.setTextColor(Color.RED);
                    }
                    break;
                }
                case "4":{
                    try {
                        c2.setTime(df.parse(c.getDayend()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int a = c1.get(Calendar.DAY_OF_YEAR);
                    int b = c2.get(Calendar.DAY_OF_YEAR);
                    if (c2.get(Calendar.YEAR)!=c1.get(Calendar.YEAR) ) priority.setText("อีก "+Integer.toString(365-a+b)+" วัน");
                    else priority.setText("อีก "+Integer.toString(b-a)+" วันตรวจสอบการเป็นสัด");
                    priority.setTextColor(Color.parseColor("#00b200"));
                    break;
                }
                case "5":{
                    try {
                        c2.setTime(df.parse(c.getDayend()));
                    } catch (ParseException e) {
//                        e.printStackTrace();
                    }
                    int a = c1.get(Calendar.DAY_OF_YEAR);
                    int b = c2.get(Calendar.DAY_OF_YEAR);
                    if (c2.get(Calendar.YEAR)!=c1.get(Calendar.YEAR) ) priority.setText("อีก "+Integer.toString(365-a+b)+" วัน");
                    else priority.setText("อีก "+Integer.toString(b-a)+" วันตรวจสอบการเป็นสัด");
                    priority.setTextColor(Color.parseColor("#E69500"));
                    break;
                }
                case "6":{
                    priority.setText("รอยืนยันสถานะคลอด");
                    priority.setTextColor(Color.RED);
                    break;
                }
            }
            return view;
        }

        @Override
        public boolean isChildSelectable(int i, int i1) {
            return true;
        }

    }

}