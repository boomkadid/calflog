package com.kasidid.mrbboomm.calflog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.net.URLEncoder;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 11/22/2016.
 */

public class Log_Milk  extends Activity {
    COWHelper mHelper;
    private MyAdapter myAdapter;
    Cow c;
    ListView mListView;
    MilkHelper lHelper;
    List<String> ids;

    @Override
    protected void onResume() {
        super.onResume();
        ids = lHelper.getMilkArray(c.getCowname(),0,c.getParity());
        myAdapter = new MyAdapter(this,ids);
        mListView.setAdapter(myAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_milk);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*.8),(int)(height*.8));

        Intent intent = getIntent();
        String id = intent.getStringExtra("ID");
        mHelper = new COWHelper(this);
        c = mHelper.getCowByID(id);
        mListView= (ListView)findViewById(R.id.listmilk);
        lHelper = new MilkHelper(this);


    }
    class MyAdapter extends BaseAdapter {
        List<String> menu;


        private Context context;
        public MyAdapter(Context context,List<String> menu){
            this.context = context;
            this.menu = menu;
        }
        @Override
        public int getCount() {
            return this.menu.size();
        }

        @Override
        public Object getItem(int i) {
            return this.menu.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            View row = null;
            if (view == null){
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.milk_item,viewGroup,false);
            }
            else{
                row = view;
            }
            final Milk milk = lHelper.getMilkByID(ids.get(position));
            TextView text = (TextView) row.findViewById(R.id.milk);
            Button delete = (Button)row.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(Log_Milk.this)
                            .setMessage("คุณต้องการลบข้อมูล?")
                            .setNegativeButton("ยกเลิก", null)
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    lHelper.deleteMilk(ids.get(position));
                                    new Sync_DeleteMilk(Log_Milk.this, milk).execute();
                                    startActivity(getIntent());
                                }
                            }).create().show();
                }
            });
            text.setText("วันที่ "+c.getDayTH(milk.getMilkdate())+"\n"+
            "ปริมาณน้ำนม "+milk.getVolume()+" กก.");
            return row;
        }
    }
    public class Sync_DeleteMilk extends AsyncTask<String, Void, String> {
        Context mContext;
        COWHelper mHelper;
        Milk milk;

        public Sync_DeleteMilk(Context context, Milk milk) {
            this.mContext = context;
            this.milk = milk;
        }

        protected String doInBackground(String... params) {
            String link;
            String data;
            try {
                data = "?username=" + URLEncoder.encode(this.milk.getUsername(), "UTF-8");
                data += "&cowname=" + URLEncoder.encode(this.milk.getCowname(), "UTF-8");
                data += "&milkdate=" + URLEncoder.encode(this.milk.getMilkdate(), "UTF-8");
                link = "https://madlab.cpe.ku.ac.th/cowapp/manager/deletemilk.php" + data;
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                return "success";


            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        protected void onPostExecute(String result) {

        }


    }

}