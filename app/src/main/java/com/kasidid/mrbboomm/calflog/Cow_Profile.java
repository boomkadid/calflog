package com.kasidid.mrbboomm.calflog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.layernet.thaidatetimepicker.date.DatePickerDialog;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mrbboomm on 8/19/2016.
 */
public class Cow_Profile extends ActionBarActivity implements DatePickerDialog.OnDateSetListener{

    private Toolbar mToolbar;
    private SystemBarTintManager mStatusBarManager;
    COWHelper mHelper;
    float[] milk_volume;
    Cow cow;
    String username;
    CALVINGHelper calvingHelper;
    MilkHelper milkHelper;
    PDFView pdfView;
    TextView name_,cowid_,birth_,fraction_,papa_,mama_,parity_,name,cowid,birth,fraction,papa,mama,total,avg,report,parity,calving_,calving,number,age,age_;
    ImageView image_profile;
    EditText drydate;
    @Override
    protected void onResume() {
        super.onResume();
        milk_volume = milkHelper.getMilkVolume(this,cow.getCowname(), cow.getParity());
        calvingHelper = new CALVINGHelper(this);
        if (cow.getType().equals("1")){ //cow
            if (!calvingHelper.getReport(cow.getCowname(),cow.getParity()).get(2).equals("-")){
                number.setText("พักรีดนมวันที่ "+cow.getDayTH(calvingHelper.getReport(cow.getCowname(),cow.getParity()).get(2)));
                number.setTextSize(22);
                number.setTextColor(Color.RED);
                total.setVisibility(View.GONE);
                avg.setVisibility(View.GONE);
                findViewById(R.id.dry).setVisibility(View.GONE);
            }
            else {
                avg.setText("น้ำนมเฉลี่ย: " + String.format("%,.1f", milk_volume[0])+" กก./วัน");
                total.setText("น้ำนมรวมทั้งหมด: " + String.format("%,.1f", milk_volume[1])+" กก.");
                number.setText("จำนวนวันให้นม " + (int) milk_volume[2] + " วัน");
            }
            //show report
            String reports = "";
//            BREEDHelper breedHelper = new BREEDHelper(this);
            for (int i = 0; i < Integer.parseInt(cow.getParity()); i++) {
                String parity = String.valueOf(i+1);
                float[] milk_volume = milkHelper.getMilkVolume(this,cow.getCowname(), parity);
                if (milk_volume[1] == 0){continue;}
//                List<String> breeds = breedHelper.getBreedTimes(cow.getCowname(), parity);
                CBreed cBreed = new CBREEDHelper(this).getCBreed(cow.getCowname(), parity);
                List<String> calving = calvingHelper.getReport(cow.getCowname(), parity);
                Log.d("calvingdate",calving.get(0));
//                reports += "ท้องที่: " + parity +
//                        "\nผสมครั้งที่: " + breeds.get(0) + " พ่อพันธุ์: " + breeds.get(1) +
//                        "\nคลอดวันที่: " + cow.getDayTH(calving.get(0)) + " เพศลูก: " + calving.get(1) +
//                        "\nน้ำนมรวมทั้งหมด: " + String.format("%,.1f", milk_volume[1])+" กก. " + " น้ำนมเฉลี่ย: " + String.format("%,.1f", milk_volume[0])+" กก./วัน" + "\n" +
//                        "น้ำนมรวมที่ 305 วัน: " + String.format("%,.1f",(milk_volume[0] * 305)) +" กก."+ "\n";
                reports += "ท้องที่: " + parity +
                        "\nผสมครั้งที่: " + cBreed.getTimes() + " พ่อพันธุ์: " + cBreed.getPapa() +
                        "\nคลอดวันที่: " + cow.getDayTH(calving.get(0)) + " เพศลูก: " + calving.get(1) +
                        "\nน้ำนมรวมทั้งหมด: " + String.format("%,.1f", milk_volume[1])+" กก. " + " น้ำนมเฉลี่ย: " + String.format("%,.1f", milk_volume[0])+" กก./วัน" + "\n" +
                        "น้ำนมรวมที่ 305 วัน: " + String.format("%,.1f",(milk_volume[0] * 305)) +" กก."+ "\n";

            }
            report.setText(reports);

        }
        else if (cow.getType().equals("0")){ // huffer
            avg.setText(null);
            total.setText(null);
            number.setText("ยังไม่มีการให้ผลผลิตน้ำนม");
            number.setTextColor(Color.RED);
            number.setTextSize(22);
            findViewById(R.id.add).setVisibility(View.GONE);
            findViewById(R.id.show).setVisibility(View.GONE);
            findViewById(R.id.dry).setVisibility(View.GONE);
            findViewById(R.id.report).setVisibility(View.GONE);
            findViewById(R.id.parity).setVisibility(View.GONE);
            findViewById(R.id.parity_).setVisibility(View.GONE);
            findViewById(R.id.calvingdate).setVisibility(View.GONE);
            findViewById(R.id.calvingdate_).setVisibility(View.GONE);

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        username =  prefs.getString("username","not found");
        setContentView(R.layout.cow_profile);
        final Intent intent = getIntent();
        final String id = intent.getStringExtra("ID");
        mHelper = new COWHelper(this);

        cow = mHelper.getCowByID(id);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        name = (TextView)findViewById(R.id.name);
        cowid = (TextView)findViewById(R.id.cowid);
        birth = (TextView)findViewById(R.id.birth);
        fraction = (TextView)findViewById(R.id.fraction);
        papa = (TextView)findViewById(R.id.papa);
        mama = (TextView)findViewById(R.id.mama);
        name_ = (TextView)findViewById(R.id.name_);
        cowid_ = (TextView)findViewById(R.id.cowid_);
        birth_ = (TextView)findViewById(R.id.birth_);
        fraction_ = (TextView)findViewById(R.id.fraction_);
        papa_ = (TextView)findViewById(R.id.papa_);
        mama_ = (TextView)findViewById(R.id.mama_);
        total = (TextView)findViewById(R.id.total);
        avg = (TextView)findViewById(R.id.avg);
        report = (TextView)findViewById(R.id.report);
        parity = (TextView)findViewById(R.id.parity);
        parity_ = (TextView)findViewById(R.id.parity_);
        age = (TextView)findViewById(R.id.age);
        age_ = (TextView)findViewById(R.id.age_);
        calving = (TextView)findViewById(R.id.calvingdate);
        calving_ = (TextView)findViewById(R.id.calvingdate_);
        number = (TextView)findViewById(R.id.number);
        image_profile = (ImageView) findViewById(R.id.profile_image);

        name.setText(cow.getNickname());
        cowid.setText(cow.getCowname());
        birth.setText(cow.getDayTH(cow.getBirth()));
        fraction.setText(cow.getFraction());
        papa.setText(cow.getPapa());
        mama.setText(cow.getMama());
        parity.setText(cow.getParity());
        try{
            CALVINGHelper cHelper = new CALVINGHelper(this);
            Calving calv = cHelper.getCalving(cow.getCowname(),cow.getParity());
            calving.setText(cow.getDayTH(calv.getCalvingdate()));
        }
        catch(Exception e) {
            calving.setText(cow.getDayTH(cow.getCalvdate()));
        }
        Picasso.with(this).load("https://madlab.cpe.ku.ac.th/cowapp/manager/images/"+cow.getUsername()+"/"+cow.getCowname()+"/1.png").error(R.drawable.cow_acc).into(image_profile);
        String fontPath = "fonts/THSarabun.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(),fontPath);
        String fontPath2 = "fonts/THSarabun Bold.ttf";
        Typeface tf2 = Typeface.createFromAsset(getAssets(),fontPath2);
        name.setTypeface(tf);
        cowid.setTypeface(tf);
        birth.setTypeface(tf);
        fraction.setTypeface(tf);
        papa.setTypeface(tf);
        mama.setTypeface(tf);
        parity.setTypeface(tf);
        name_.setTypeface(tf2);
        cowid_.setTypeface(tf2);
        birth_.setTypeface(tf2);
        fraction_.setTypeface(tf2);
        papa_.setTypeface(tf2);
        mama_.setTypeface(tf2);
        calving.setTypeface(tf);
        calving_.setTypeface(tf2);
        parity_.setTypeface(tf2);
        age.setTypeface(tf);
        age_.setTypeface(tf2);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar today = Calendar.getInstance();
        Calendar birth = Calendar.getInstance();
        try {
            birth.setTime(df.parse(cow.getBirth()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int diffYear = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
        int diffMonth = today.get(Calendar.MONTH) - birth.get(Calendar.MONTH);
        if (diffMonth <0) { diffMonth += 12; diffYear -= 1;}
        if (diffYear < 1) age.setText("อายุ: "+diffMonth+" เดือน");
        else age.setText(diffYear+" ปี "+diffMonth+" เดือน");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(cow.getNickname());
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mStatusBarManager = new SystemBarTintManager(this);
        mStatusBarManager.setStatusBarTintEnabled(true);

//===================================================================================
        mHelper = new COWHelper(this);
        milkHelper = new MilkHelper(this);

        avg.setTypeface(tf);
        total.setTypeface(tf);
        number.setTypeface(tf);
        report.setTypeface(tf);
        Button add = (Button)findViewById(R.id.add);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Cow_Profile.this,Cow_Profile_Milk_Add.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        Button show = (Button)findViewById(R.id.show);
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Cow_Profile.this,Log_Milk.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        Button showPDF = (Button)findViewById(R.id.reportcow);
        showPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Cow_Profile.this,report_Cow.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        ImageView transfer = (ImageView)findViewById(R.id.transfer);
        transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressDialog();
                new Sync_GetUser(Cow_Profile.this,"test",cow.getCowname()).execute();

            }
        });
        Button dry = (Button)findViewById(R.id.dry);
        dry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Cow_Profile.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dry_dialog, null);
                dialogBuilder.setView(dialogView);

                drydate = (EditText) dialogView.findViewById(R.id.dry_date);
                drydate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        Calendar now = Calendar.getInstance();
                        DatePickerDialog dpd = DatePickerDialog.newInstance(
                                Cow_Profile.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)

                        );
                        dpd.setAccentColor("#6388ad");
                        dpd.show(getFragmentManager(), "Datepickerdialog");
                    }
                });
                dialogBuilder.setMessage("กรุณาใส่วันพักรีดนม");
                dialogBuilder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //do something with edt.getText().toString();
                        new AlertDialog.Builder(Cow_Profile.this)
                                .setMessage("ต้องการพักรีดนม")
                                .setNegativeButton("ยกเลิก",new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Calving calving = calvingHelper.getCalving(cow.getCowname(),cow.getParity());
                                        calving.setRestday(drydate.getText().toString());
                                        calvingHelper.updateCalving(calving);
                                        new Sync_Calving(calving).execute();
                                        startActivity(getIntent());
                                        finish();
                                    }
                                }).create().show();

                        dialog.dismiss();
                    }
                });
                dialogBuilder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                        dialog.dismiss();
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });
        show.setTypeface(tf);
        showPDF.setTypeface(tf);
        add.setTypeface(tf);
        dry.setTypeface(tf);


    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        drydate.setText(date);
    }
    private ProgressDialog mProgressDialog;
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
    public class Sync_GetUser extends AsyncTask<String,Void,String>{
        Context mContext;
        String farmid, data, cowname;
        ArrayList<String> usernames = new ArrayList<>();
        ArrayList<String> owners = new ArrayList<>();
        public Sync_GetUser(Context context,String FarmID, String cowname_){
            mContext = context;
            farmid = FarmID;
            cowname = cowname_;
        }
        @Override
        protected String doInBackground(String... params) {
            data= "";

            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("http://madlab.cpe.ku.ac.th/cowapp/manager/transfer/getmember.php" + data).build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("result(farm)",result);

                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            usernames.add(x.getString("username"));
                            owners.add(x.getString("owner"));
                            Log.d("get(farm)", "done");

                        } catch (Exception e) {

                            Log.d("get(farm)","fail");
                            continue;
                        }

                    }
                }
                return "success";
            }catch (Exception e){

                Log.d("get(farm)","fail");
            }
            return "fail";
        }

        @Override
        protected void onPostExecute(String s) {
            hideProgressDialog();
            if (s.equals("success")){

                Intent intent = new Intent(Cow_Profile.this,Transfer_toUser.class);
                intent.putExtra("usernames", usernames);
                intent.putExtra("owners",  owners);
                intent.putExtra("cowname",  cowname);
                startActivity(intent);
                finish();
            }
            else{
                Toast.makeText(Cow_Profile.this, "กรุณาเชื่อมต่ออินเตอร์เน็ต", Toast.LENGTH_LONG).show();

            }
        }
    }

}